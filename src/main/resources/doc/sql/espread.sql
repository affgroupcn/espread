/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80017
 Source Host           : localhost:3306
 Source Schema         : espread

 Target Server Type    : MySQL
 Target Server Version : 80017
 File Encoding         : 65001

 Date: 17/10/2019 13:01:43
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `BLOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CALENDAR_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `CALENDAR_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CRON_EXPRESSION` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TIME_ZONE_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
INSERT INTO `qrtz_cron_triggers` VALUES ('scheduler', 'Test_A', 'Test', '0/10 * * * * ?', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ENTRY_ID` varchar(95) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `INSTANCE_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `FIRED_TIME` bigint(13) NOT NULL,
  `SCHED_TIME` bigint(13) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `JOB_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `ENTRY_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DESCRIPTION` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IS_DURABLE` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IS_NONCONCURRENT` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IS_UPDATE_DATA` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO `qrtz_job_details` VALUES ('scheduler', 'Test_A', 'Test', NULL, 'com.espread.sys.service.impl.TaskTest', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000B7363686564756C654A6F6273720024636F6D2E657370726561642E7379732E6D6F64656C2E5379735363686564756C654A6F6200000000000000010200064C0009636C6173734E616D657400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000B6465736372697074696F6E71007E00094C000567726F757071007E00094C00046E616D6571007E00094C000673746174757371007E00097870740025636F6D2E657370726561642E7379732E736572766963652E696D706C2E5461736B5465737474000E302F3130202A202A202A202A203F7074000454657374740006546573745F41740001317800);

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `LOCK_NAME` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `LOCK_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('scheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `INSTANCE_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `LAST_CHECKIN_TIME` bigint(13) NOT NULL,
  `CHECKIN_INTERVAL` bigint(13) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `INSTANCE_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
INSERT INTO `qrtz_scheduler_state` VALUES ('scheduler', 'DESKTOP-92H8TTH1571287441567', 1571288478802, 15000);

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `REPEAT_COUNT` bigint(7) NOT NULL,
  `REPEAT_INTERVAL` bigint(12) NOT NULL,
  `TIMES_TRIGGERED` bigint(10) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `STR_PROP_1` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STR_PROP_2` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STR_PROP_3` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `INT_PROP_1` int(11) NULL DEFAULT NULL,
  `INT_PROP_2` int(11) NULL DEFAULT NULL,
  `LONG_PROP_1` bigint(20) NULL DEFAULT NULL,
  `LONG_PROP_2` bigint(20) NULL DEFAULT NULL,
  `DEC_PROP_1` decimal(13, 4) NULL DEFAULT NULL,
  `DEC_PROP_2` decimal(13, 4) NULL DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DESCRIPTION` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(13) NULL DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(13) NULL DEFAULT NULL,
  `PRIORITY` int(11) NULL DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_TYPE` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `START_TIME` bigint(13) NOT NULL,
  `END_TIME` bigint(13) NULL DEFAULT NULL,
  `CALENDAR_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `MISFIRE_INSTR` smallint(2) NULL DEFAULT NULL,
  `JOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `SCHED_NAME`(`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `qrtz_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
INSERT INTO `qrtz_triggers` VALUES ('scheduler', 'Test_A', 'Test', 'Test_A', 'Test', NULL, 1571288490000, 1571288480000, 5, 'WAITING', 'CRON', 1571288475000, 0, NULL, 0, '');

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
  `dict_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `dict_pid` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dict_type` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dict_code` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dict_name` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dict_flag` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dict_seq` int(11) NULL DEFAULT NULL,
  `description` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `UK_sys_dict_TYPE:CODE`(`dict_type`, `dict_code`) USING BTREE,
  INDEX `FK_sys_dict_PID`(`dict_pid`) USING BTREE,
  CONSTRAINT `FK_sys_dict_PID` FOREIGN KEY (`dict_pid`) REFERENCES `sys_dict` (`dict_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('1', NULL, 'logType', '', '日志类型', NULL, NULL, '');
INSERT INTO `sys_dict` VALUES ('11', '1', 'logType', 'system', '系统日志', NULL, 1, NULL);
INSERT INTO `sys_dict` VALUES ('12', '1', 'logType', 'login', '登陆日志', NULL, 0, '');
INSERT INTO `sys_dict` VALUES ('2', NULL, 'userState', '', '用户状态', NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES ('21', '2', 'userState', '1', '正常', NULL, 0, NULL);
INSERT INTO `sys_dict` VALUES ('22', '2', 'userState', '0', '停用', NULL, 2, '');
INSERT INTO `sys_dict` VALUES ('41', '1', 'logType', 'exception', '异常日志', NULL, 3, '');

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`  (
  `log_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `login_name` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL,
  `log_type` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `log_flag` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `operate_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `request_param` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `execute_time` bigint(20) NULL DEFAULT NULL,
  `description` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `os` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `browser` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ip` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `mac` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `field_1` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `field_2` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `field_3` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`log_id`) USING BTREE,
  INDEX `FK_sys_log_USER_ID`(`user_id`) USING BTREE,
  CONSTRAINT `FK_sys_log_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `sys_user` (`user_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_org
-- ----------------------------
DROP TABLE IF EXISTS `sys_org`;
CREATE TABLE `sys_org`  (
  `org_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `org_pid` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `org_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `org_address` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `org_status` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `org_type` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `org_level` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `icon` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `org_seq` int(11) NULL DEFAULT NULL,
  `description` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `field_1` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `field_2` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `field_3` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`org_id`) USING BTREE,
  INDEX `FK_sys_org_PID`(`org_pid`) USING BTREE,
  CONSTRAINT `FK_sys_org_PID` FOREIGN KEY (`org_pid`) REFERENCES `sys_org` (`org_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_org
-- ----------------------------
INSERT INTO `sys_org` VALUES ('600008', NULL, '总行', '', NULL, NULL, NULL, NULL, 1, '', NULL, NULL, NULL);
INSERT INTO `sys_org` VALUES ('600009', '600008', '分行', '', NULL, NULL, NULL, NULL, 1, '', '123', '32', '44');
INSERT INTO `sys_org` VALUES ('600010', '600008', '分行1', '', NULL, NULL, NULL, NULL, 2, '', NULL, NULL, NULL);
INSERT INTO `sys_org` VALUES ('600011', '600008', '分行2', '', NULL, NULL, NULL, NULL, 3, '', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_resource
-- ----------------------------
DROP TABLE IF EXISTS `sys_resource`;
CREATE TABLE `sys_resource`  (
  `resource_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `resource_pid` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `resource_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `resource_type` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `perm_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `url` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `icon` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `resource_status` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1',
  `resource_level` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `resource_flag` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `resource_seq` int(11) NULL DEFAULT NULL,
  `description` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `field_1` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `field_2` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `field_3` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`resource_id`) USING BTREE,
  INDEX `FK_sys_resource_PID`(`resource_pid`) USING BTREE,
  CONSTRAINT `FK_sys_resource_PID` FOREIGN KEY (`resource_pid`) REFERENCES `sys_resource` (`resource_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_resource
-- ----------------------------
INSERT INTO `sys_resource` VALUES ('1', NULL, '系统维护', '1', NULL, '', 'icon-home', '1', '1', NULL, 1, '', NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('103', '4', '删除', '2', 'sys:resource:del', '', '', '1', NULL, NULL, 3, '', NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('124', '4', '编辑', '2', 'sys:resource:edit', '', '', '1', NULL, NULL, 2, '', NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('139', '2', '添加', '2', 'sys:role:add', NULL, '', '1', NULL, NULL, 1, '', NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('142', '2', '修改', '2', 'sys:role:edit', '', '', '1', NULL, NULL, 3, '', NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('143', '2', '授权', '2', 'sys:role:grant', '', '', '1', NULL, NULL, 4, '角色分配菜单', NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('144', '2', '删除', '2', 'sys:role:del', '', '', '1', NULL, NULL, 4, '', NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('151', NULL, '系统管理', '1', NULL, '', 'icon-home', '1', '1', NULL, 2, '', NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('152', '3', '添加', '2', 'sys:user:add', NULL, '', '1', NULL, NULL, 1, '', NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('153', '3', '修改', '2', 'sys:user:edit', '', '', '1', NULL, NULL, 2, '', NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('154', '3', '删除', '2', 'sys:user:del', '', '', '1', NULL, NULL, 3, '', NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('162', '3', '查看', '2', 'sys:user:view', NULL, '', '1', NULL, NULL, 0, '', NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('164', '2', '查看', '2', 'sys:role:view', '', '', '1', NULL, NULL, 0, '', NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('165', '4', '查看', '2', 'sys:resource:view', '', '', '1', '3', NULL, 0, '', NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('166', '3', '修改角色', '2', 'sys:user:editRole', '', '', '1', NULL, NULL, 3, '', NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('190', '1', '机构管理', '1', NULL, '/sysOrg', 'icon-standard-chart-organisation', '1', '2', NULL, 3, '', NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('191', '190', '查看', '2', 'sys:org:view', '', '', '1', '3', NULL, 0, '', NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('192', '190', '新增', '2', 'sys:org:add', '', '', '1', '3', NULL, 1, '', NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('193', '190', '修改', '2', 'sys:org:edit', '', '', '1', '3', NULL, 2, '', NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('194', '190', '删除', '2', 'sys:org:del', '', '', '1', '3', NULL, 3, '', NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('2', '1', '角色管理', '1', NULL, '/sysRole', 'icon-standard-report-user', '1', '2', NULL, 2, '', NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('200', '151', '数据源监控', '1', '', '/druid', '', '1', NULL, NULL, 2, '', NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('201', '151', '字典管理', '1', NULL, '/sysDict', '', '1', NULL, NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('202', '201', '查看', '2', 'sys:dict:view', '', '', '1', NULL, NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('203', '201', '新增', '2', 'sys:dict:add', '', '', '1', NULL, NULL, 1, '', NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('204', '201', '修改', '2', 'sys:dict:edit', '', '', '1', NULL, NULL, 2, '', NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('205', '201', '删除', '2', 'sys:dict:del', '', '', '1', NULL, NULL, 3, '', NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('207', '1', '日志查询', '1', NULL, '/sysLog', 'icon-cologne-publish', '1', NULL, NULL, 4, '', NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('208', '207', '查看', '2', 'sys:log:view', NULL, '', '1', NULL, NULL, 0, '', NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('3', '1', '用户管理', '1', '', '/sysUser', 'icon-cologne-customers', '1', '2', NULL, 1, '', NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('4', '151', '资源管理', '1', '', '/sysResource', 'icon-folder', '1', '2', NULL, 1, '', NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('5', '4', '添加', '2', 'sys:resource:add', '', '', '1', '3', NULL, 1, '', NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('5355f8d8c7c241789e7015287cc285bf', '151', 'cron表达式生成', '1', NULL, '/sysScheduleJob/quartzCron', 'icon-hamburg-future', '1', NULL, NULL, 3, '', NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('ccfda310fb0f401eb9bca61e2923da5a', '151', '定时任务管理', '1', NULL, '/sysScheduleJob', '', '1', NULL, NULL, 4, '', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `role_pid` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `role_name` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `role_status` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1',
  `role_level` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `role_flag` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `icon` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `role_seq` int(11) NULL DEFAULT NULL,
  `description` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `field_1` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `field_2` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `field_3` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`role_id`) USING BTREE,
  INDEX `FK_sys_role_PID`(`role_pid`) USING BTREE,
  CONSTRAINT `FK_sys_role_PID` FOREIGN KEY (`role_pid`) REFERENCES `sys_role` (`role_id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', NULL, '系统管理员', '1', NULL, NULL, NULL, 1, '', NULL, NULL, NULL);
INSERT INTO `sys_role` VALUES ('2', NULL, '普通用户', '1', NULL, NULL, NULL, 3, '', NULL, NULL, NULL);
INSERT INTO `sys_role` VALUES ('27', NULL, '来宾用户', '1', NULL, NULL, NULL, 0, '', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_role_resource
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_resource`;
CREATE TABLE `sys_role_resource`  (
  `role_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `resource_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`role_id`, `resource_id`) USING BTREE,
  INDEX `FK_sys_role_resource_RESOURCE_ID`(`resource_id`) USING BTREE,
  CONSTRAINT `FK_sys_role_resource_RESOURCE_ID` FOREIGN KEY (`resource_id`) REFERENCES `sys_resource` (`resource_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_sys_role_resource_ROLE_ID` FOREIGN KEY (`role_id`) REFERENCES `sys_role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_resource
-- ----------------------------
INSERT INTO `sys_role_resource` VALUES ('1', '1');
INSERT INTO `sys_role_resource` VALUES ('2', '1');
INSERT INTO `sys_role_resource` VALUES ('1', '103');
INSERT INTO `sys_role_resource` VALUES ('1', '124');
INSERT INTO `sys_role_resource` VALUES ('1', '139');
INSERT INTO `sys_role_resource` VALUES ('1', '142');
INSERT INTO `sys_role_resource` VALUES ('2', '142');
INSERT INTO `sys_role_resource` VALUES ('1', '143');
INSERT INTO `sys_role_resource` VALUES ('2', '143');
INSERT INTO `sys_role_resource` VALUES ('1', '144');
INSERT INTO `sys_role_resource` VALUES ('2', '144');
INSERT INTO `sys_role_resource` VALUES ('1', '151');
INSERT INTO `sys_role_resource` VALUES ('1', '152');
INSERT INTO `sys_role_resource` VALUES ('2', '152');
INSERT INTO `sys_role_resource` VALUES ('1', '153');
INSERT INTO `sys_role_resource` VALUES ('2', '153');
INSERT INTO `sys_role_resource` VALUES ('1', '154');
INSERT INTO `sys_role_resource` VALUES ('2', '154');
INSERT INTO `sys_role_resource` VALUES ('1', '162');
INSERT INTO `sys_role_resource` VALUES ('2', '162');
INSERT INTO `sys_role_resource` VALUES ('1', '164');
INSERT INTO `sys_role_resource` VALUES ('1', '165');
INSERT INTO `sys_role_resource` VALUES ('1', '166');
INSERT INTO `sys_role_resource` VALUES ('1', '190');
INSERT INTO `sys_role_resource` VALUES ('1', '191');
INSERT INTO `sys_role_resource` VALUES ('1', '192');
INSERT INTO `sys_role_resource` VALUES ('1', '193');
INSERT INTO `sys_role_resource` VALUES ('1', '194');
INSERT INTO `sys_role_resource` VALUES ('1', '2');
INSERT INTO `sys_role_resource` VALUES ('2', '2');
INSERT INTO `sys_role_resource` VALUES ('1', '200');
INSERT INTO `sys_role_resource` VALUES ('1', '201');
INSERT INTO `sys_role_resource` VALUES ('1', '202');
INSERT INTO `sys_role_resource` VALUES ('1', '203');
INSERT INTO `sys_role_resource` VALUES ('1', '204');
INSERT INTO `sys_role_resource` VALUES ('1', '205');
INSERT INTO `sys_role_resource` VALUES ('1', '207');
INSERT INTO `sys_role_resource` VALUES ('1', '208');
INSERT INTO `sys_role_resource` VALUES ('1', '3');
INSERT INTO `sys_role_resource` VALUES ('2', '3');
INSERT INTO `sys_role_resource` VALUES ('1', '4');
INSERT INTO `sys_role_resource` VALUES ('1', '5');
INSERT INTO `sys_role_resource` VALUES ('1', '5355f8d8c7c241789e7015287cc285bf');
INSERT INTO `sys_role_resource` VALUES ('1', 'ccfda310fb0f401eb9bca61e2923da5a');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `login_name` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `salt` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_status` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_level` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_flag` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `org_id` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sex` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `birth_date` datetime(0) NULL DEFAULT NULL,
  `phone` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `address` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `login_count` int(11) NULL DEFAULT NULL,
  `lastvisit_date` datetime(0) NULL DEFAULT NULL,
  `create_date` datetime(0) NULL DEFAULT NULL,
  `field_1` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `field_2` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `field_3` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `field_4` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `field_5` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', 'ddb90e7a439cd901dcbf4265cc811940795f108b', '835ef259e8dfa221', 'espread_m', '1', NULL, NULL, '600008', '1', NULL, '18292972913', '7121867@qq.com', '111', '11111111111', 81, '2019-10-17 12:59:47', '2012-12-12 02:20:10', '', '', NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES ('2', '123', '8e2ed686a785115b1fa6a78593ca941d12b208f0', '3fdc8ec7c8e16a68', '234', '1', NULL, NULL, '600009', '1', NULL, '', '', '', '', NULL, NULL, '2016-09-04 16:31:51', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES ('3', '002', 'fa477831b26d8920ac369f602f02eb67e61f0e2c', '49706ae0c0841b56', '002', '1', NULL, NULL, '600010', '1', NULL, '', '', '', '', NULL, NULL, '2016-09-04 22:03:36', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES ('5', '001', '7f6de4aa53c1bad8aac9684a9f6a5182400fc54f', '5aa2d2d8b53e7ee7', '001', '1', NULL, NULL, NULL, '1', NULL, '', '', '', '', NULL, NULL, '2016-08-21 21:05:31', NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `role_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE,
  INDEX `FK_sys_user_role_ROLE_ID`(`role_id`) USING BTREE,
  CONSTRAINT `FK_sys_user_role_ROLE_ID` FOREIGN KEY (`role_id`) REFERENCES `sys_role` (`role_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `FK_sys_user_role_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `sys_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1');
INSERT INTO `sys_user_role` VALUES ('2', '1');
INSERT INTO `sys_user_role` VALUES ('3', '1');
INSERT INTO `sys_user_role` VALUES ('1', '2');
INSERT INTO `sys_user_role` VALUES ('3', '2');
INSERT INTO `sys_user_role` VALUES ('5', '2');
INSERT INTO `sys_user_role` VALUES ('3', '27');

SET FOREIGN_KEY_CHECKS = 1;
